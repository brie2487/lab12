package ro.ubb.catalog.web.controller;


import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Assignment;
import ro.ubb.catalog.core.service.AssignmentService;
import ro.ubb.catalog.core.service.ProblemService;
import ro.ubb.catalog.core.service.StudentService;
import ro.ubb.catalog.web.converter.AssignmentConverter;
import ro.ubb.catalog.web.converter.ProblemConverter;
import ro.ubb.catalog.web.converter.StudentConverter;
import ro.ubb.catalog.web.dto.AssignmentDto;
import ro.ubb.catalog.web.dto.AssignmentsDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class AssignmentController {

    private static final Logger log = LoggerFactory.getLogger(AssignmentController.class);


    @Autowired
    private AssignmentService assignmentService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private ProblemService problemService;
    @Autowired
    private AssignmentConverter assignmentConverter;
    @Autowired
    private StudentConverter studentConverter;
    @Autowired
    private ProblemConverter problemConverter;



    @Transactional
    @RequestMapping(value="/assignments/{sid}", method=RequestMethod.POST)
    ResponseEntity saveAssignment(@PathVariable Long sid, @RequestBody Long pid){
        log.trace("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgot in saveAssignment**************************************************************************************");

        System.out.println("in controller");
        System.out.println(sid);
        System.out.println(pid);
        this.assignmentService.addAssignment(sid, pid);

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }



    @RequestMapping(value="/assignments/{sid}", method= RequestMethod.GET)
    public Set<AssignmentDto> getAllAssignmentsOfStudent(@PathVariable final Long sid){
        Set<Assignment> assignments =assignmentService.filterByStudent(sid);
        Set<AssignmentDto> result=assignmentConverter.convertModelsToDtos(assignments);
        System.out.println("into getAllOfAssigns");
        System.out.println(result.size());
        result.forEach(s-> {System.out.println(s.getSid());
            System.out.println(
                s.getPid());});
        return result;
    }

    @RequestMapping(value="/assignments", method=RequestMethod.GET)
    List<AssignmentDto> getAssignments(){
       Set<Assignment> assignments=assignmentService.getAllAssignments();
       return new ArrayList<>(assignmentConverter.convertModelsToDtos(assignments));
    }


    @Transactional
    @RequestMapping(value="/assignments/{sid}", method=RequestMethod.PUT)
    public ResponseEntity updateAssignment(@PathVariable Long sid,@RequestBody final AssignmentDto dto){
        System.out.println("ctrl: update");
        assignmentService.updateGrade(dto.getSid(), dto.getPid(), dto.getGrade());
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value="/assignments/del/{sid}", method=RequestMethod.POST)
    public ResponseEntity<?> deleteAssignment(@PathVariable Long sid, @RequestBody Long pid){
        System.out.println("in console. delete");
        assignmentService.deleteAssignment(sid, pid);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }




}
