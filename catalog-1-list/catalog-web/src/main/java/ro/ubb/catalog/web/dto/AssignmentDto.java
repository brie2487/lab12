package ro.ubb.catalog.web.dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
@Builder
public class AssignmentDto {
    private Long sid;
    private Long pid;
    private Integer grade;

}
