package ro.ubb.catalog.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.service.ProblemService;
import ro.ubb.catalog.web.converter.ProblemConverter;
import ro.ubb.catalog.web.dto.ProblemDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProblemController {
    private static final Logger log = LoggerFactory.getLogger(ProblemController.class);

    @Autowired
    private ProblemService problemService;

    @Autowired
    private ProblemConverter problemConverter;

    @RequestMapping(value="/problems", method= RequestMethod.GET)
    public List<ProblemDto> getProblems(){
        log.trace("getProblems");

        List<Problem> problems = problemService.findAll();

        log.trace("getProblems: problems={}", problems);

        return new ArrayList<>(problemConverter.convertModelsToDtos(problems));
    }

    @RequestMapping(value="/problems/{problemId}", method=RequestMethod.PUT)
    public ProblemDto updateProblem(@PathVariable final Long problemId, @RequestBody final ProblemDto problemDto){
        log.trace("updateStudent: id={}, problemDtoMap={}", problemId, problemDto);

        Problem problem=problemService.updateProblem(problemId,
                problemDto.getSerialNumber(),
                problemDto.getDescr());

        ProblemDto problemDto1=problemConverter.convertModelToDto(problem);
        return problemDto1;
    }

    @RequestMapping(value="/problems", method=RequestMethod.POST)
    ProblemDto saveProblem(@RequestBody ProblemDto problemDto){
        return problemConverter.convertModelToDto(problemService.saveProblem(problemConverter.convertDtoToModel(problemDto)));

    }



    @RequestMapping(value="/problems/{problemId}", method=RequestMethod.DELETE)
    ResponseEntity<?> deleteById(@PathVariable Long problemId){
        problemService.deleteProblem(problemId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
