package ro.ubb.catalog.web.converter;


import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.web.dto.ProblemDto;

import java.util.stream.Collectors;

@Component
public class ProblemConverter extends AbstractConverter<Problem, ProblemDto>{
    @Override
    public Problem convertDtoToModel(ProblemDto dto) {
//        Problem problem = new Problem();
//        problem.setDescr(dto.getDescr());
//        problem.setSerialNumber(dto.getSerialNumber());
//        problem.setId(dto.getId());
//        return problem;

        Problem problem=Problem.builder()
                .serialNumber(dto.getSerialNumber())
                .descr(dto.getDescr())
                .build();
        problem.setId(dto.getId());
        return problem;
    }

    @Override
    public ProblemDto convertModelToDto(Problem problem) {
//        ProblemDto problemDto=new ProblemDto(problem.getSerialNumber(), problem.getDescr());
//        problemDto.setId(problem.getId());
//        return problemDto;

        ProblemDto problemDto=ProblemDto.builder()
                .serialNumber(problem.getSerialNumber())
                .descr(problem.getDescr())
                .students(problem.getStudents().stream()
                .map(BaseEntity::getId).collect(Collectors.toSet())).build();

        problemDto.setId(problem.getId());
        return problemDto;



    }
}
