package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.web.dto.StudentDto;

import java.util.stream.Collectors;

/**
 * Created by radu.
 */

@Component
public class StudentConverter extends AbstractConverter<Student, StudentDto> {

    private static final Logger log = LoggerFactory.getLogger(StudentConverter.class);

    @Override
    public Student convertDtoToModel(StudentDto dto) {
//        Student student=new Student();
//        student.setSerialNumber(dto.getSerialNumber());
//        student.setName(dto.getName());
//        student.setGroupNumber(dto.getGroupNumber());
//
//        student.setId(dto.getId());
//        return student;

        Student student=Student.builder()
                .serialNumber(dto.getSerialNumber())
                .name(dto.getName())
                .groupNumber(dto.getGroupNumber())
                .build();

        student.setId(dto.getId());
        return student;

    }

    @Override
    public StudentDto convertModelToDto(Student student) {
        StudentDto studentDto = StudentDto.builder()
                .serialNumber(student.getSerialNumber())
                .name(student.getName())
                .groupNumber(student.getGroupNumber())
                .problems(student.getProblems().stream()
                        .map(BaseEntity::getId).collect(Collectors.toSet()))
                .build();
        studentDto.setId(student.getId());
        return studentDto;
    }
}
