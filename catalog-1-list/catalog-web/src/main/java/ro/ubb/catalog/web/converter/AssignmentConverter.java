package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Assignment;
import ro.ubb.catalog.web.dto.AssignmentDto;

@Component
public class AssignmentConverter extends AbstractConverter<Assignment, AssignmentDto> {
    @Override
    public Assignment convertDtoToModel(AssignmentDto assignmentDto) {
        return null;
    }

    @Override
    public AssignmentDto convertModelToDto(Assignment assignment) {
        return AssignmentDto.builder()
                .sid(assignment.getStudent().getId())
                .pid(assignment.getProblem().getId())
                .grade(assignment.getGrade()).build();

    }
}
