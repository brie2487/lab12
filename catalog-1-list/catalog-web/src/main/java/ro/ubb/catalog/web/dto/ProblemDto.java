package ro.ubb.catalog.web.dto;


import lombok.*;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProblemDto extends BaseDto{
    private String serialNumber;
    private String descr;
    private Set<Long> students;

    @Override
    public String toString() {
        return "ProblemDto{" +
                "serialNumber='" + serialNumber + '\'' +
                ", descr='" + descr + '\'' +
                '}';
    }

}
