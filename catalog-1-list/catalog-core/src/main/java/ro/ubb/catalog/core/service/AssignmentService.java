package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Assignment;

import java.util.Set;

public interface AssignmentService {

//
//
//    public void addAssignment(Long clientID,Long BookID);
//
//    public Set<Order> getAllOrders();
//
////    public void deleteClientID(Long id);
//
//    public Set<Order> filterByClient(Long id);
//
////    public void deleteBookID(Long id);
//
//    public Set<Order> filterByBook(Long id);
//
//    public void deleteOrder(Long clientID,Long bookID);
//
//    public void updateOrder(Long clientID,Long bookID, Double price);
//
//    public Order find(Long clientID,Long bookID);


    public void addAssignment(Long studentID, Long problemID);

    public Set<Assignment> getAllAssignments();

    public Set<Assignment> filterByStudent(Long id);
    public Set<Assignment> filterByProblem(Long id);
    public void deleteAssignment(Long sid, Long pid);
    public void updateGrade(Long sid, Long pid, Integer grade);
    public Assignment find(Long sid, Long pid);

}
