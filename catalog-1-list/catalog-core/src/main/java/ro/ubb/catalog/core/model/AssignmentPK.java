package ro.ubb.catalog.core.model;


import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
class AssignmentPK implements Serializable {
    private Student student;
    private Problem problem;
}
