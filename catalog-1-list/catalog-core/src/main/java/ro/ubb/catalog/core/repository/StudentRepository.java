package ro.ubb.catalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import ro.ubb.catalog.core.model.Student;

import java.util.List;

public interface StudentRepository extends CatalogRepository<Student, Long> {
//    @Query(value="SELECT s from Student s order by s.id asc");
//    List<Student> findAllStudents();
}
