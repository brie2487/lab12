package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Problem;

import java.util.List;

public interface ProblemService {

    List<Problem> findAll();
    Problem saveProblem(Problem problem);
    Problem updateProblem(Long problemID, String serialNumber, String descr);
    void deleteProblem(Long id);



}
