package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Assignment;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.repository.ProblemRepository;
import ro.ubb.catalog.core.repository.StudentRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class AssignmentServiceImpl implements AssignmentService {
    public static final Logger log = LoggerFactory.getLogger(AssignmentServiceImpl.class);

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ProblemRepository problemRepository;


    @Override
    @Transactional
    public void addAssignment(Long studentID, Long problemID) {
        Optional<Student> student=studentRepository.findById(studentID);
        Optional<Problem> problem=problemRepository.findById(problemID);
        if(student.isPresent() && problem.isPresent()){
            student.get().addProblem(problem.get());
        }
        //return new Assignment(student.get(), problem.get(), 0);
    }

    @Override
    public Set<Assignment> getAllAssignments() {
        Set<Assignment> all=new HashSet<Assignment>();
        studentRepository.findAll().forEach(s->all.addAll(s.getAssignmentSet()));
        return all;

    }



    @Override
    public Set<Assignment> filterByStudent(Long id) {
        Optional<Student> student=studentRepository.findById(id);
        System.out.println(student.map(Student::getAssignmentSet));
        return student.map(Student::getAssignmentSet).orElse(null);

    }

    @Override
    public Set<Assignment> filterByProblem(Long id) {
        Optional<Problem> problem=problemRepository.findById(id);
        return problem.map(Problem::getAssignmentSet).orElse(null);
    }

    @Override
    @Transactional
    public void deleteAssignment(Long sid, Long pid) {
        Optional<Student> student=studentRepository.findById(sid);
        Optional<Problem> problem=problemRepository.findById(pid);
        if(student.isPresent() && problem.isPresent()){
            problem.get().deleteStudentForProblem(sid);
            student.get().deleteProblemForStudent(pid);
        }
    }

    @Override
    @Transactional
    public void updateGrade(Long sid, Long pid, Integer grade) {
        Optional<Student> student = studentRepository.findById(sid);
        Optional<Problem> problem = problemRepository.findById(pid);
        if (student.isPresent() && problem.isPresent()) {
            student.get().getAssignmentSet().stream().filter(as->as.getProblem().getId().equals(pid)).forEach(as->as.setGrade(grade));
        }
    }

    @Override
    public Assignment find(Long sid, Long pid) {
        Optional<Student> student=studentRepository.findById(sid);
        if(student.isPresent()){
            return student.get().getAssignmentSet().stream().filter(as->as.getProblem().getId().equals(pid)).collect(Collectors.toList()).get(0);
        }
        return null;
    }
}
