package ro.ubb.catalog.core.model;

import lombok.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Student extends BaseEntity<Long> {
    @Column(name = "serialNumber")
    private String serialNumber;
    @Column(name="name")
    private String name;
    @Column(name="groupNumber")
    private Integer groupNumber;

    @Override
    public String toString() {
        return "Student{" +
                "serialNumber='" + serialNumber + '\'' +
                ", name='" + name + '\'' +
                ", groupNumber=" + groupNumber +
                "} " + super.toString();
    }

    @OneToMany(mappedBy="student", cascade= CascadeType.ALL, fetch= FetchType.EAGER, orphanRemoval = true)
    private Set<Assignment> assignmentSet=new HashSet<>();

    public Set<Problem> getProblems(){
        assignmentSet=assignmentSet == null ? new HashSet<>() : assignmentSet;

        return Collections.unmodifiableSet(
                this.assignmentSet.stream().map(Assignment :: getProblem).collect(Collectors.toSet())
        );
    }


    @Transactional
    public void addProblem(Problem problem){
        Assignment assignment=new Assignment();
        assignment.setStudent(this);
        assignment.setProblem(problem);
        assignment.setGrade(0);
        this.assignmentSet.add(assignment);
    }

    public void addProblems(Set<Problem> problems){problems.forEach(this::addProblem);}

    public void addGrade(Problem problem, Integer grade){
        Assignment assignment=new Assignment();

        assignment.setStudent(this);
        assignment.setProblem(problem);
        assignment.setGrade(grade);
        assignmentSet.add(assignment);
    }


    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return serialNumber.equals(student.serialNumber);
    }

    @Override
    public int hashCode() {
        return serialNumber.hashCode();
    }

    @Modifying
    @Transactional
    public void deleteProblemForStudent(Long id){
        this.assignmentSet.removeIf(s->s.getProblem().getId().equals(id));
    }
}
