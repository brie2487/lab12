package ro.ubb.catalog.core.model;


import lombok.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
@Builder
public class Problem extends BaseEntity<Long> {
    @Column(name="serialNumber", nullable=false)
    private String serialNumber;

    @Column(name="descr")
    private String descr;


    @OneToMany(mappedBy="problem", cascade= CascadeType.ALL, fetch= FetchType.EAGER, orphanRemoval = true)
    private Set<Assignment> assignmentSet=new HashSet<>();

    public Set<Student> getStudents(){

        assignmentSet=assignmentSet == null ? new HashSet<>() : assignmentSet;

        return Collections.unmodifiableSet(
                this.assignmentSet.stream().map(Assignment :: getStudent).collect(Collectors.toSet())
        );
    }

    public void addStudent(Student student) {
        Assignment assignment = new Assignment();
        assignment.setStudent(student);
        assignment.setProblem(this);
        assignmentSet.add(assignment);
    }

    @Modifying
    @Transactional
    public void deleteStudentForProblem(Long id){
        this.assignmentSet.removeIf(p->p.getStudent().getId().equals(id));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Problem that = (Problem) o;

        return serialNumber.equals(that.serialNumber);
    }

    @Override
    public int hashCode() {
        return serialNumber.hashCode();
    }


    @Override
    public String toString() {
        return "Problem{" +
                "serialNumber='" + serialNumber + '\'' +
                ", descr='" + descr + '\'' +
                '}';
    }
}
