package ro.ubb.catalog.core.service;

import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.core.repository.ProblemRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class ProblemServiceImpl implements ProblemService {
    private static final Logger log = LoggerFactory.getLogger(ProblemServiceImpl.class);
    @Autowired
    ProblemRepository problemRepository;

    @Override
    public List<Problem> findAll() {
        log.trace("findAll --- method entered");
        List<Problem> problems=problemRepository.findAll();
        log.trace("findAll: problems={}", problems);
        return problems;
    }

    @Override
    public Problem saveProblem(Problem problem) {
        return problemRepository.save(problem);
    }

    @Override
    @Transactional
    public Problem updateProblem(Long problemID, String serialNumber, String descr) {
        log.trace("updateProblem: serialNumber={}, descr={}", serialNumber, descr);
        Optional<Problem> problem=problemRepository.findById(problemID);
        problem.ifPresent(p->{p.setSerialNumber(serialNumber); p.setDescr(descr); });
        log.trace("updateProblem: problem={}", problem.get());
        return problem.orElse(null);
    }

    @Transactional
    @Override
    public void deleteProblem(Long id) {
        problemRepository.findById(id).get().getStudents().stream().forEach(s->s.deleteProblemForStudent(id));
        problemRepository.deleteById(id);
        System.out.println("delete in serviceImpl...");
        System.out.println(id);
    }
}
