package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "assignment")
@IdClass(AssignmentPK.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString

public class Assignment implements Serializable {
    @Id
    @ManyToOne(optional=false, fetch=FetchType.EAGER)
    @JoinColumn(name="student_id")
    private Student student;


    @Id
    @ManyToOne(optional=false, fetch=FetchType.EAGER)
    @JoinColumn(name="problem_id")
    private Problem problem;

    @Column(name="grade")
    private Integer grade;

}
